var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ProjectSchema = new Schema(
    {
  name: {
    type: String,
    
    required: true,
  },
  description: {
    type: String,
   
  },
    useremail: {
    type: String,
  
  },
  imageurl: {
    type: String,
 
  },
  
}, { timestamps: { createdAt: 'created_at' } });




module.exports = mongoose.model("Project", ProjectSchema);

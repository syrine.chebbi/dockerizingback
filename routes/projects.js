var express = require('express');
var router = express.Router();
var Project = require("./../model/project");
var passport = require("passport");
require("../config/passport")(passport);

/* GET users listing. */
router.get('/all',passport.authenticate("jwt", { session: false }), async function (req, res, next) {
 
  try {
    const listProject= await Project.find();
    res.status(200).send(listProject)
  } catch (error) {
    res.status(405).send("server error")
  }
});

/* POST new user */
router.post('/save',passport.authenticate("jwt", { session: false }), async function (req, res, next) {
    console.log("body",req.body);
  try {
    const newProject = new Project(req.body);
    console.log("project",newProject);
    await newProject.save();
    res.status(200).send(newProject)
  } catch (error) {
    res.status(405).send("server error")
  }

});
/*Delete project */
router.delete('/delet/:id',passport.authenticate("jwt", { session: false }), async function (req, res, next) {
  try {
    const {id} = req.params.id;
    console.log('id:', id);

    const project = await Project.findByIdAndDelete({ _id: req.params.id }).exec();
    if (!project) {
      return res.status(401).send({
        success: false,
        msg: "Delete failed. Project non trouve.",
      });
    } else{
      return res.status(200).send({success: true,
      msg:"delete with sucsess",
    })

    }
    
  } catch (error) {
    console.log('error', error.stack);
    res.status(405).send("server error");
  }
});


/* POST new project */
router.get('/:email',passport.authenticate("jwt", { session: false }), (req, res) => {
  Project.find({ useremail: req.params.email })
    .then(project => res.json(project))
    .catch(err => res.status(404).json({ nobookfound: 'No user found' }));
});

/* POST new project */
router.get('/findProject/:idproject',passport.authenticate("jwt", { session: false }), (req, res) => {
  Project.find({ _id: req.params.idproject })
    .then(project => res.json(project))
    .catch(err => res.status(404).json({ nobookfound: 'No user found' }));
});






module.exports = router;

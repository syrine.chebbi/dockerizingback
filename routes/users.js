var express = require('express');
var router = express.Router();
var transporter = require('./../config/mailer');
var User = require("./../model/user");
var jwt = require("jsonwebtoken");

/* GET users listing. */
router.get('/all', async function (req, res, next) {
  //  User.find(function(err,data){
  //  })
  try {
    const listUser = await User.find();
    res.status(200).send(listUser)
  } catch (error) {
    res.status(405).send("server error")
  }
});

/* POST new user */
router.post('/save', async function (req, res, next) {
  try {
    const newUser = new User(req.body);
    await newUser.save();
    res.status(200).send(newUser)
  } catch (error) {
    res.status(405).send("server error")
  }

});

/* POST users login. */
router.post('/login', async function (req, res, next) {
  
 // try {
    const user = await User.findOne({ email: req.body.email }).exec();
    if (!user) {
      return res.status(401).send({
        success: false,
        msg: "Authentication failed. User non trouve.",
      });
    }
    const isMatch = await user.comparePassword(req.body.password);
    if (isMatch) {
      var token = jwt.sign(user.toJSON(), "a60dc1ec1cqsdqsdqsd774624f3e96e146124059d", {
        expiresIn: 86400, // un jour
      });
      return res.json({ loggedIn: true, User: user, token: "JWT " + token });    } else {
      res.status(401).send("user not found")
    }
 // } catch (error) {
 //   res.status(405).send("server error")
 // }
});


/*  send foprget password email  */
router.post('/forget-password', async function (req, res) {

  const user = await User.findOne({ email: req.body.email }).exec();
  if (!user) {
    return res.status(401).send({
      success: false,
      msg: "this email is not found",
    });
  }

  user.verif_code = Math.floor(Math.random() * Math.floor(999999));
  await user.save();

  var mailOptions = {
    from: 'sassou.pim@gmail.com',
    to: user.email,
    subject: 'forget possword',
    text: user.verif_code
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
      res.status(405).send("error server");
    } else {
      console.log('Email sent: ' + info.response);
      res.status(200).send("email was sent");
    }
  });
})


/*  send foprget password   */
router.post('/forget-password/change', async function (req, res) {

  try {
    const user = await User.findOne({ email: req.body.email }).exec();
    if (!user) {
      return res.status(401).send({
        success: false,
        msg: "this email is not found",
      });
    }

    if (user.verif_code != req.body.verif_code) {
      return res.status(401).send({
        success: false,
        msg: "Access denied",
      });
    }

    user.password = req.body.password;
    await user.save();

    res.status(200).send("password has been changed");

  } catch (error) {
    res.status(405).send("server error")
  }



})


module.exports = router;
